﻿using CurriculumTimetabling.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using TimeTablingProblem.Models;
using TimeTablingProblem.Models.Enums;

namespace CurriculumTimetabling
{
    class IO
    {
        public static void Read(string path)
        {
            var doc = new XmlDocument();
            doc.Load(path);
            var root = doc.DocumentElement;
            var parentNode = root.SelectSingleNode("descriptor");
            var courseDetails = new List<CourseDetail>();

            foreach (XmlNode node in parentNode.ChildNodes)
            {
                if (node.Name == "days")
                {
                    InputInstance.Descriptor.Days = Convert.ToInt32(node.Attributes["value"].Value);
                    break;
                }
            }

            parentNode = root.SelectSingleNode("courses");
            foreach (XmlNode node in parentNode.ChildNodes)
            {
                var course = new Course
                {
                    Id = node.Attributes["id"].Value,
                    Teacher = node.Attributes["teacher"].Value,
                    Lectures = Convert.ToInt32(node.Attributes["lectures"].Value),
                    Students = Convert.ToInt32(node.Attributes["students"].Value),
                    MinDays = Convert.ToInt32(node.Attributes["min_days"].Value),
                    DoubleLectures = node.Attributes["double_lectures"].Value == "yes" ? true : false
                };
                InputInstance.Courses.Add(course);
                courseDetails.Add(new CourseDetail { CourseId = course.Id, TeacherId = course.Teacher, TotalLectures = course.GetTotalLectures(), TotalStudents = course.Students });
            }

            Solution.CourseDetails.AddRange(courseDetails);

            parentNode = root.SelectSingleNode("rooms");
            foreach (XmlNode node in parentNode.ChildNodes)
            {
                InputInstance.Rooms.Add(new Room
                {
                    Id = node.Attributes["id"].Value,
                    Size = Convert.ToInt32(node.Attributes["size"].Value),
                    Building = Convert.ToByte(node.Attributes["building"].Value),
                });
            }

            parentNode = root.SelectSingleNode("constraints");
            foreach (XmlNode node in parentNode.ChildNodes)
            {
                var courseId = node.Attributes["course"].Value;
                var type = node.Attributes["type"].Value == "period" ? ConstraintType.Period : ConstraintType.Room;

                var constraint = new Constraint { Type = type, CourseId = courseId, TimeSlots = new List<TimeSlot>(), Rooms = new List<Room>() };

                foreach (XmlNode childNode in node.ChildNodes)
                {
                    if (type == ConstraintType.Period)
                    {
                        var timeslot = new TimeSlot
                        {
                            Day = Convert.ToInt32(childNode.Attributes["day"].Value),
                            Period = Convert.ToInt32(childNode.Attributes["period"].Value)
                        };
                        constraint.TimeSlots.Add(timeslot);
                        Solution.CourseDetails.Find(x => x.CourseId == courseId).RestricTimeSlots.Add(timeslot);

                    }
                    else
                    {
                        var room = InputInstance.Rooms.Find(rm => rm.Id == childNode.Attributes["ref"].Value);
                        if (room != null)
                        {
                            constraint.Rooms.Add(room);
                            Solution.CourseDetails.Find(x => x.CourseId == courseId).RestrictRoomIds.Add(room.Id);
                        }
                    }
                }

                InputInstance.Constraints.Add(constraint);
            }

            parentNode = root.SelectSingleNode("curricula");
            foreach (XmlNode node in parentNode.ChildNodes)
            {
                var curricula = new Curriculum { Id = node.Attributes["id"].Value, Courses = new List<Course>() };
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    var course = InputInstance.Courses.Find(crs => crs.Id == childNode.Attributes["ref"].Value);

                    if (course != null)
                    {
                        curricula.Courses.Add(course);
                        //Solution.CourseDetails.Find(x => x.CourseId == childNode.Attributes["ref"].Value).CurriculmIds.Add(node.Attributes["id"].Value);
                    }
                }
                InputInstance.Curricula.Add(curricula);
            }
        }

        public static void Write(List<Assignment> assignments, string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(path))
            {
                foreach (var assignment in assignments)
                {
                    streamWriter.WriteLine(assignment.CourseId + " " + assignment.RoomId + " " + assignment.Day + " " + assignment.PeriodStart);
                }
            }
        }
    }
}

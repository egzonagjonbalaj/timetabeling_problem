using CurriculumTimetabling.Models;
using System;

namespace CurriculumTimetabling
{
    class Program
    {
        static void Main(string[] args)
        {
            var instanceName = "UP-FME-AS2017";
            IO.Read($@"Input\{instanceName}.xml");

            //var solution = Solution.GenerateRandom();

            var ils = new IteratedLocalSearch();
            var solution = ils.GenerateSolution();

            var assignments = solution.Assignments;

            var cost = solution.Evaluate();
            IO.Write(assignments, $@"Output\{instanceName}.txt");

            Console.WriteLine("CourseId RoomId Day Start");
            foreach (var item in assignments)
            {
                Console.WriteLine($"{item.CourseId} {item.RoomId} {item.Day} {item.PeriodStart}");
            }
            Console.ReadKey();
        }
    }
}

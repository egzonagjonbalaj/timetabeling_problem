﻿namespace TimeTablingProblem.Models
{
    public class Course
    {
        public string Id { get; set; } 
        //public int CId { get; set; }
        public string Teacher { get; set; }
        public int Lectures { get; set; }
        public int MinDays { get; set; }
        public int Students { get; set; }
        public bool DoubleLectures { get; set; }

        public int GetTotalLectures()
        {
            var period = 3;
            switch (Lectures)
            {
                case 1:
                case 2:
                    period = period * 2;
                    break;
                case 3:
                case 4:
                    period = period * Lectures + 1;
                    break;
                case 5:
                case 6:
                    period = period * Lectures + 2;
                    break;
                default:
                    break;
            }
            
            return period;
        }

        private int CalculateLecturePeriods()
        {
            var total = 0;
            var period = 3;

            var remainder = Lectures % 2;
            if (remainder == 0)
            {
                var pause = Lectures / 2;
                total = period * Lectures + pause - 1;
            }
            else
            {
                var pause = Lectures / 2;
                total = period * Lectures + pause;
            }
            return total;
        }
    }
}

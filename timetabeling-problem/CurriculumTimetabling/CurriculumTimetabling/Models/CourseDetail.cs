﻿using System.Collections.Generic;
using TimeTablingProblem.Models;

namespace CurriculumTimetabling.Models
{
    class CourseDetail
    {
        public string CourseId { get; set; }
        public string TeacherId { get; set; }
        public int TotalLectures { get; set; }
        public int TotalStudents { get; set; }
        public List<string> RestrictRoomIds { get; set; }
        public List<TimeSlot> RestricTimeSlots { get; set; }
        //public List<string> CurriculmIds { get; set; }
        public CourseDetail()
        {
            RestrictRoomIds = new List<string>();
            RestricTimeSlots = new List<TimeSlot>();
            //CurriculmIds = new List<string>();
        }
    }
}

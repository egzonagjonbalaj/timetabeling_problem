﻿using System.Collections.Generic;
using TimeTablingProblem.Models.Enums;

namespace TimeTablingProblem.Models
{
    class BaseConstraint
    {
        public ConstraintType Type { get; set; }
        public string CourseId { get; set; }
    }
    
    class Constraint : BaseConstraint
    {
        public List<TimeSlot> TimeSlots { get; set; }
        public List<Room> Rooms { get; set; }
    }

    class PeriodConstraint : BaseConstraint
    {
        public List<TimeSlot> TimeSlots { get; set; }
    }

    class RoomConstraint : BaseConstraint
    {
        public List<Room> Rooms { get; set; }
    }
}

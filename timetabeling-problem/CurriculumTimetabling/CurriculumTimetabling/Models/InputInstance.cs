﻿using System.Collections.Generic;
using TimeTablingProblem.Models;

namespace CurriculumTimetabling.Models
{
    class InputInstance
    {
        public static Descriptor Descriptor { get; set; } = new Descriptor();
        public static List<Course> Courses { get; set; } = new List<Course>();
        public static List<Room> Rooms { get; set; } = new List<Room>();
        public static List<Curriculum> Curricula { get; set; } = new List<Curriculum>();
        public static List<Constraint> Constraints { get; set; } = new List<Constraint>();
        //public static List<PeriodConstraint> PeriodConstraints { get; set; }
        //public static List<RoomConstraint> RoomConstraints { get; set; }
    }
}

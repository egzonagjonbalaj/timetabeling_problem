﻿using System;
using System.Timers;

namespace CurriculumTimetabling.Models
{
    class IteratedLocalSearch
    {
        public bool Running { get; set; }
        void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Running = false;
        }

        public Solution GenerateSolution()
        {
            var r = new Timer(120000);
            r.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            r.Enabled = true;
            Running = true;
            Random rnd = new Random();

            Solution S = Solution.GenerateRandom();
            Console.WriteLine($"Initial penalty: {S.Evaluate()}");
            Solution H = S.Copy();
            Solution Best = H.Copy();

            while (Running)
            {
                for (int climb_iterations = 0; climb_iterations < 10; climb_iterations++)
                {
                    var R = S.Copy();
                    R.ChangeRoomMutation();
                    if (S.Evaluate() < R.Evaluate())
                    {
                        S = R;
                    }
                }

                if (S.Evaluate() > Best.Evaluate())
                {
                    Best = S;
                }

                H = NewHomeBase(H, S);
                S = H.Copy();
                S.Perturb();
            }
            r.Enabled = false;

            return Best;
        }

        public Solution NewHomeBase(Solution H, Solution S)
        {
            return S.Evaluate() >= H.Evaluate() ? S : H;
        }
    }
}

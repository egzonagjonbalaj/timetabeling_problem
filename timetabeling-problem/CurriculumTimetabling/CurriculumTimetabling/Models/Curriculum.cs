﻿using System.Collections.Generic;

namespace TimeTablingProblem.Models
{
    class Curriculum
    {
        public string Id { get; set; }
        //public int QId { get; set; } 
        public List<Course> Courses { get; set; } 
    }
}

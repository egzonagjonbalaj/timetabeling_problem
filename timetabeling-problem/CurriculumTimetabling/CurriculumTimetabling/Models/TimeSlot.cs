﻿namespace TimeTablingProblem.Models
{
    class TimeSlot
    {
        public int Day { get; set; }
        public int Period { get; set; }
    }

    public class Period
    {
        public int Start { get; set; }
        public int End { get; set; }

        public override string ToString()
        {
            return this.Start + "  " + this.End;
        }
    }
}

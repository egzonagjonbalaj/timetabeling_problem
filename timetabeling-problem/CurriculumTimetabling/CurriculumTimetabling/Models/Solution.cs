﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTablingProblem.Models;
using TimeTablingProblem.Models.Enums;

namespace CurriculumTimetabling.Models
{
    class Solution
    {
        public List<Assignment> Assignments { get; set; }
        public List<CourseDetail> CourseDetailsCopy { get; set; }
        public static List<CourseDetail> CourseDetails { get; set; } = new List<CourseDetail>();
        private int MaxPeriodPerDay => 48;
        private int Days => InputInstance.Descriptor.Days;

        public Solution()
        {
            Assignments = new List<Assignment>();
            CourseDetailsCopy = new List<CourseDetail>();
        }

        public Solution Copy()
        {
            var solution = new Solution
            {
                Assignments = new List<Assignment>(this.Assignments),
                CourseDetailsCopy = new List<CourseDetail>(this.CourseDetailsCopy)
            };

            return solution;
        }

        private bool IsTeacherAvaliable(string teacherId, int day, Period period, string courseId = null)
        {
            var assignments = Assignments.Where(x => x.TeacherId == teacherId && x.Day == day).OrderBy(x => x.PeriodStart).ToList();
            if (assignments.Count > 0)
            {
                if (courseId != null)
                {
                    var courseAssignment = Assignments.Find(x => x.CourseId == courseId);
                    assignments.Remove(courseAssignment);
                }

                foreach (var item in assignments)
                {
                    var end = item.PeriodEnd - item.PeriodStart + 1;
                    var assignmentPeriods = Enumerable.Range(item.PeriodStart, end);
                    var choosenAssignmentPeriod = Enumerable.Range(period.Start, period.End - period.Start + 1);
                    if (assignmentPeriods.Intersect(choosenAssignmentPeriod).Any())
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool IsInSameCurriculum(string courseId, int day, Period period)
        {
            if (!Assignments.Any(x => x.Day == day))
                return false;

            var course = InputInstance.Courses.First(x => x.Id == courseId);
            var curriculaCourses = InputInstance.Curricula.Where(x => x.Courses.Contains(course)).ToList();
            var courses = curriculaCourses.SelectMany(x => x.Courses).Distinct().ToList();
            courses.Remove(course);

            var assignments = new List<Assignment>();
            foreach (var item in courses)
            {
                var courseAssigned = Assignments.FirstOrDefault(x => x.CourseId == item.Id && x.Day == day);
                if (courseAssigned != null)
                {
                    assignments.Add(courseAssigned);
                }
            }

            if (assignments.Count == 0)
                return false;
            foreach (var item in assignments)
            {
                var end = item.PeriodEnd - item.PeriodStart + 1;

                var assignmentPeriods = Enumerable.Range(item.PeriodStart, end);

                var choosenAssignmentPeriod = Enumerable.Range(period.Start, period.End - period.Start + 1);

                if (assignmentPeriods.Intersect(choosenAssignmentPeriod).Any())
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsRoomAvaliable(string roomId, Period period, int day, string courseId = null)
        {
            var assignments = Assignments.Where(x => x.RoomId == roomId && x.Day == day).OrderBy(x => x.PeriodStart).ToList();
            if (assignments.Count > 0)
            {
                if (courseId != null)
                {
                    var courseAssignment = Assignments.Find(x => x.CourseId == courseId);
                    assignments.Remove(courseAssignment);
                }

                foreach (var item in assignments)
                {
                    var end = item.PeriodEnd - item.PeriodStart + 1;

                    var assignmentPeriods = Enumerable.Range(item.PeriodStart, end);

                    var choosenAssignmentPeriod = Enumerable.Range(period.Start, period.End - period.Start + 1);

                    if (assignmentPeriods.Intersect(choosenAssignmentPeriod).Any())
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool AssignCourse(CourseDetail course, string roomId, int day)
        {
            var totalPeriods = course.TotalLectures;
            var restricedTimeSlots = course.RestricTimeSlots.Where(x => x.Day == day).OrderBy(x => x.Period).Select(x => x.Period).ToList();

            var max = MaxPeriodPerDay - totalPeriods;


            var assignmentsForDay = Assignments.Where(x => x.Day == day && x.RoomId == roomId).OrderBy(x => x.PeriodStart).Select(x =>
                                   new Period { Start = x.PeriodStart, End = x.PeriodEnd }).ToList();

            for (int i = 0; i < max; i++)
            {
                var range = Enumerable.Range(i, i + totalPeriods - 1);

                if (restricedTimeSlots.Intersect(range).Any())
                {
                    i = restricedTimeSlots.Last();
                    continue;
                }
                if (assignmentsForDay.Any(x => Enumerable.Range(x.Start, x.End - x.Start + 1).Contains(i)))
                    continue;
                var period = new Period { Start = i, End = i + totalPeriods - 1 };

                var isTeacherAvaliable = IsTeacherAvaliable(course.TeacherId, day, period);
                if (isTeacherAvaliable)
                {
                    var isInSameCurriculum = IsInSameCurriculum(course.CourseId, day, period);
                    if (!isInSameCurriculum)
                    {
                        var isRoomAvaliable = IsRoomAvaliable(roomId, period, day);
                        if (isRoomAvaliable)
                        {
                            var roomSize = InputInstance.Rooms.Find(x => x.Id == roomId).Size;
                            var c = InputInstance.Courses.Find(x => x.Id == course.CourseId);
                            var curriculaCourses = InputInstance.Curricula.Where(x => x.Courses.Contains(c)).Select(x => x.Id).ToList();

                            Assignments.Add(new Assignment
                            {
                                CourseId = course.CourseId,
                                RoomId = roomId,
                                Day = day,
                                PeriodStart = period.Start,
                                PeriodEnd = period.End,
                                TeacherId = course.TeacherId,
                                HasRoomCapacityViolation = roomSize < course.TotalStudents ? true : false,
                                CurriculaIds = curriculaCourses
                            });
                            return true;
                        }
                    }
                }
                continue;
            }
            return false;
        }

        public static Solution GenerateRandom()
        {
            var solution = new Solution { CourseDetailsCopy = new List<CourseDetail>(CourseDetails) };
            var random = new Random();
            var curriculaCourses = InputInstance.Curricula.GroupBy(x => new { x.Id, x.Courses, Total = x.Courses.Count }).ToList();
            while (CourseDetails.Count > 0)
            {
                var randomCurricula = random.Next(0, curriculaCourses.Count);
                var curriculaId = curriculaCourses[randomCurricula].Select(x => x.Id).First();
                var chosenCurriculaCourses = curriculaCourses[randomCurricula].SelectMany(x => x.Courses).ToList();
                var ids = chosenCurriculaCourses.Select(x => x.Id);

                if (solution.Assignments.Any(x => ids.Contains(x.CourseId)))
                {
                    var all = solution.Assignments.Where(x => ids.Contains(x.CourseId)).ToList();
                    foreach (var item in all)
                    {
                        var y = chosenCurriculaCourses.Find(x => x.Id == item.CourseId);
                        chosenCurriculaCourses.Remove(y);
                    }
                }

                if (chosenCurriculaCourses.Count == 0)
                {
                    var x = curriculaCourses[randomCurricula];
                    curriculaCourses.Remove(x);
                    continue;
                }

                var courseRnd = random.Next(0, chosenCurriculaCourses.Count);
                var c = chosenCurriculaCourses[courseRnd];
                var choosenCourse = CourseDetails.FirstOrDefault(x => x.CourseId == c.Id);

                //var courseRnd = random.Next(0, CourseDetails.Count);
                //var choosenCourse = CourseDetails[courseRnd];
                var validRooms = GetValidRooms(choosenCourse.RestrictRoomIds);
                var validRoomRnd = random.Next(0, validRooms.Count);
                var choosenRoom = validRooms[validRoomRnd];

                var dayRnd = random.Next(0, 5);
                var isAssigned = solution.AssignCourse(choosenCourse, choosenRoom, dayRnd);
                if (isAssigned)
                    CourseDetails.Remove(choosenCourse);
            }
            return solution;
        }

        private static List<string> GetValidRooms(List<string> excludedRooms)
        {
            var roomIds = InputInstance.Rooms.Select(x => x.Id).ToList();
            return roomIds.Where(x => !excludedRooms.Contains(x)).ToList();
        }

        private List<int> GetValidDays(List<int> excludeDays)
        {
            return Enumerable.Range(0, Days).Where(i => !excludeDays.Contains(i)).ToList();
        }

        public int Evaluate()
        {
            int violations = 0;
            var roomCapacityViolations = Assignments.Where(x => x.HasRoomCapacityViolation).ToList();

            foreach (var item in roomCapacityViolations)
            {
                var roomCapacity = InputInstance.Rooms.Find(x => x.Id == item.RoomId).Size;
                var students = InputInstance.Courses.Find(x => x.Id == item.CourseId).Students;

                violations += students - roomCapacity;
            }

            var curriculas = InputInstance.Curricula.ToList();
            foreach (var c in curriculas)
            {
                for (int day = 0; day < Days; day++)
                {
                    var list = new List<Assignment>();
                    var dayAssignments = Assignments.Where(x => x.Day == day).ToList();

                    foreach (var item in dayAssignments)
                    {
                        if (c.Courses.Contains(InputInstance.Courses.First(x => x.Id == item.CourseId)))
                        {
                            list.Add(item);
                        }
                    }

                    list = list.OrderBy(x => x.PeriodStart).ToList();

                    for (int i = 1; i < list.Count; i++)
                    {
                        var window = list[i].PeriodStart - list[i - 1].PeriodEnd;
                        if (window > 1)
                        {
                            violations += window - 1;
                        }
                    }
                }
            }
            return violations;
        }

        public void Perturb()
        {
            for (int i = 0; i < 10; i++)
            {
                ChangeRoomMutation();
                //ChangeTimeSlotMutation();
            }

        }

        public void ChangeRoomMutation()
        {
            var random = new Random();
            var roomCapacityViolations = Assignments.Where(x => x.HasRoomCapacityViolation).ToList();
            var rnd = random.Next(0, roomCapacityViolations.Count);
            var choosenAssignment = roomCapacityViolations[rnd];
            var courseId = choosenAssignment.CourseId;
            var assignedCourse = Assignments.First(x => x.CourseId == courseId);
            var assignedPeriodRange = Enumerable.Range(assignedCourse.PeriodStart, assignedCourse.PeriodEnd - assignedCourse.PeriodStart + 1);
            var choosenCourse = CourseDetailsCopy.Find(x => x.CourseId == courseId);
            var suitableRooms = InputInstance.Rooms.Where(x => x.Size >= choosenCourse.TotalStudents).ToList();
            var roomIds = suitableRooms.Select(x => x.Id).ToList();
            var restrictedRooms = choosenCourse.RestrictRoomIds.ToList();
            var validRooms = roomIds.Where(x => !restrictedRooms.Contains(x)).ToList();

            foreach (var validRoomId in validRooms)
            {
                var assignments = Assignments.Where(x => x.RoomId == validRoomId && x.Day == assignedCourse.Day).ToList();
                var isAvaliable = true;
                foreach (var assignment in assignments)
                {
                    var end = assignment.PeriodEnd - assignment.PeriodStart + 1;
                    var assignmentPeriods = Enumerable.Range(assignment.PeriodStart, end);
                    if (assignmentPeriods.Intersect(assignedPeriodRange).Any())
                        isAvaliable = false;
                }
                if (isAvaliable)
                {
                    Assignments.First(x => x.CourseId == courseId).RoomId = validRoomId;
                    break;
                }
            }
        }

        public void ChangeDayMutation()
        {
            var groupedAssignments = Assignments.GroupBy(item => item.Day)
                         .Select(group => new { Day = group.Key, Assignments = group.ToList(), Total = group.Count() })
                         .ToList();
            var ordered = groupedAssignments.OrderBy(x => x.Total).ToList();

            var leastLoadedDay = ordered.First();
            var leastLoadedDayId = leastLoadedDay.Day;
            var minLoaded = leastLoadedDay.Assignments.OrderBy(x => x.PeriodStart).ToList();

            var mostLoadeDay = ordered.Last();
            var mostLoadeDayId = leastLoadedDay.Day;
            var maxLoaded = mostLoadeDay.Assignments.OrderBy(x => x.PeriodStart).ToList();

            var last = maxLoaded.Skip(Math.Max(0, maxLoaded.Count() - 10));
            var lastAssignments = new List<Assignment>(last);

            foreach (var item in lastAssignments)
            {
                if (!minLoaded.Any(x => x.PeriodStart == item.PeriodStart && x.PeriodEnd == item.PeriodEnd))
                {
                    var courseId = item.CourseId;
                    var course = CourseDetailsCopy.First(x => x.CourseId == courseId);
                    var restricedTimeSlots = course.RestricTimeSlots.Where(x => x.Day == mostLoadeDayId).OrderBy(x => x.Period).Select(x => x.Period).ToList();
                    var range = Enumerable.Range(item.PeriodStart, item.PeriodEnd - item.PeriodStart + 1);
                    if (!restricedTimeSlots.Intersect(range).Any())
                    {
                        var period = new Period { Start = item.PeriodStart, End = item.PeriodEnd };

                        Assignments.First(x => x.CourseId == courseId).Day = leastLoadedDayId;
                    }
                }
            }

        }

        public void ChangeTimeSlotMutation()
        {
            var groupedAssignments = Assignments.GroupBy(item => item.Day)
               .Select(group => new { Day = group.Key, Assignments = group.ToList() })
               .ToList();

            var random = new Random();

            var day = random.Next(0, 5);
            var dayAssignments = groupedAssignments.Where(x => x.Day == day).SelectMany(y => y.Assignments).ToList();
            dayAssignments = dayAssignments.OrderBy(y => y.PeriodStart).ToList();

            var lastEnd = 0;
            foreach (var assignment in dayAssignments)
            {
                var course = CourseDetailsCopy.Find(x => x.CourseId == assignment.CourseId);
                if (lastEnd == 0)
                {
                    lastEnd = assignment.PeriodEnd;
                    continue;
                }

                if (lastEnd + 1 != assignment.PeriodStart)
                {
                    var restricedTimeSlots = course.RestricTimeSlots.Where(x => x.Day == day).OrderBy(x => x.Period).Select(x => x.Period).ToList();
                    var range = Enumerable.Range(lastEnd + 1, lastEnd + course.TotalLectures - 1);
                    if (restricedTimeSlots.Count > 0 && restricedTimeSlots.Intersect(range).Any())
                    {
                        continue;
                    }
                    else
                    {
                        var period = new Period { Start = lastEnd + 1, End = lastEnd + course.TotalLectures - 1 };
                        var isRoomAvaliable = IsRoomAvaliable(assignment.RoomId,
                            period, assignment.Day, assignment.CourseId);
                        if (isRoomAvaliable)
                        {
                            var isTeacherAvaliable = IsTeacherAvaliable(assignment.TeacherId, day, period, assignment.CourseId);
                            if (isTeacherAvaliable)
                            {
                                Assignments.Find(x => x.CourseId == course.CourseId).PeriodStart = period.Start;
                                Assignments.Find(x => x.CourseId == course.CourseId).PeriodStart = period.End;
                                break;
                            }
                        }
                    }
                }
                lastEnd = assignment.PeriodEnd;
            }
        }
    }
}

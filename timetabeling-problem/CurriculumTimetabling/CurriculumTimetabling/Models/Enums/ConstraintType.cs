﻿namespace TimeTablingProblem.Models.Enums
{
    enum ConstraintType
    {
        Period = 1,
        Room = 2
    }
}

﻿namespace TimeTablingProblem.Models
{
    class Descriptor
    {
        public int Days { get; set; }
        public int PeriodPerDay { get; set; }
        public DailyLecture DailyLectures { get; set; }
    }

    public class DailyLecture 
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
}

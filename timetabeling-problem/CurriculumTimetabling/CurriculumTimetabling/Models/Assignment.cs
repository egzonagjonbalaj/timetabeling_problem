﻿using System.Collections.Generic;

namespace CurriculumTimetabling.Models
{
    class Assignment
    {
        public string CourseId { get; set; }
        public string RoomId { get; set; }
        public string TeacherId { get; set; }
        public int Day { get; set; }
        public int PeriodStart { get; set; }
        public int PeriodEnd { get; set; }
        public bool HasRoomCapacityViolation { get; set; }
        public List<string> CurriculaIds { get; set; }

        public Assignment()
        {
            CurriculaIds = new List<string>();
        }
        public override string ToString()
        {
            return this.CourseId + " " + this.RoomId + " " + this.Day + " " + this.PeriodStart + "  " + this.PeriodEnd + "  " + string.Join(",", this.CurriculaIds);
        }
    }
}
